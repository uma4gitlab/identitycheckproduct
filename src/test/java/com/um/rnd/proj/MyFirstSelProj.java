package com.um.rnd.proj;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MyFirstSelProj {
	
	private WebDriver driver;
	
	@BeforeClass
	public void setUp() {
		//System.setProperty("webdriver.chrome.driver", "chromedriver");
		System.setProperty("webdriver.chrome.driver", "C:\\Uma\\SeleniumJARs\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.testandquiz.com/selenium/testing.html");
	}
	
	@Test
	public void testTopTextOfThePage () {
		
		String actual = driver.findElement(By.className("col-md-offset-2")).getText();
		String expected = "Sample WebPage for Automation Testing";
		System.out.println(actual);
		
		Assert.assertEquals(actual, expected);
	}
	
	@Test
	public void testSecondTextOfThePage () {
		
		String actual = driver.findElement(By.className("col-md-12")).getText();
		String expected = "This is sample webpage with dummy elements that will help you in learning selenium automation.";
		System.out.println(actual);
		
		Assert.assertEquals(actual, expected);
	}
	
	@Test
	public void testThirdTextOfThePage () {
		
		String actual = driver.findElement(By.xpath("/html/body/div/div[3]/div/b")).getText();
		String expected = "This is sample text.";
		System.out.println(actual);
		
		Assert.assertEquals(actual, expected);
	}
	
	@Test
	public void tesTheTextBoxAndSubmit () {
		
		WebElement we = driver.findElement(By.id("fname"));
		
		we.sendKeys("Hello how are you?");
		System.out.println(driver.findElement(By.id("idOfButton")).getText());
		driver.findElement(By.id("idOfButton")).click();
		
		WebElement web = driver.findElement(By.id("idOfButton"));
		
		Assert.assertNotNull(web);
	}
	
	@AfterClass
	public void tearDwon() throws InterruptedException{
		Thread.sleep(2000);
		driver.quit();
	}

}
