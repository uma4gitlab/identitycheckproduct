package com.ck.test.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LocationStrategyDemo {
	
	private WebDriver driver;
	
	@BeforeTest
	//@BeforeClass
	public void setUp() {
		//System.setProperty("webdriver.chrome.driver", "chromedriver");
		System.setProperty("webdriver.chrome.driver", "/Users/chittranjankumar/Documents/Ckumar/Selenium/Driver/84.0.4147.30/chromedriver");
		driver = new ChromeDriver();
		driver.get("https://www.testandquiz.com/selenium/testing.html");
	}
	
	@Test
	public void testTopTextOfThePage () {
		
		String actual = driver.findElement(By.className("col-md-offset-2")).getText();
		String expected = "Sample WebPage for Automation Testing";
		System.out.println(actual);
		
		Assert.assertEquals(actual, expected);
	}
	
	@Test
	public void testSecondTextOfThePage () {
		
		String actual = driver.findElement(By.className("col-md-12")).getText();
		String expected = "This is sample webpage with dummy elements that will help you in learning selenium automation.";
		System.out.println(actual);
		
		Assert.assertEquals(actual, expected);
	}
	
	@Test
	public void testThirdTextOfThePage () {
		
		String actual = driver.findElement(By.xpath("/html/body/div/div[3]/div/b")).getText();
		String expected = "This is sample text.";
		System.out.println(actual);
		
		Assert.assertEquals(actual, expected);
	}
	
	@Test
	public void testTheTextBoxAndSubmitById () {
		
		WebElement we = driver.findElement(By.id("fname"));
		
		we.sendKeys("Hello how are you?");
		System.out.println(driver.findElement(By.id("idOfButton")).getText());
		driver.findElement(By.id("idOfButton")).click();
		
		WebElement web = driver.findElement(By.id("idOfButton"));
		
		Assert.assertNotNull(web);
	}
	
	@Test
	public void testTheTextBoxAndSubmitByName () {
		
		WebElement we = driver.findElement(By.name("firstName"));
		//we.clear();
		we.sendKeys("Adding a text in textbox by name");
		
		driver.findElement(By.id("idOfButton")).click();
		
		Assert.assertNotNull(we);
	}
	
	@Test
	public void testTheTextBoxAndSubmitByClassName () {
		
		WebElement we = driver.findElement(By.className("Automation"));
		we.click();
		Assert.assertEquals(we.isSelected(),true);
	}
	
	@Test
	public void testTheTextboxAndByTagName () {
		
		WebElement we = driver.findElement(By.tagName("input"));
		we.sendKeys("Entering value for testing purpose");
		we.click();
		
		driver.findElement(By.tagName("button")).click();
		Assert.assertEquals(true, true);
	}
	
	@Test
	public void testTheTextboxAndByLinkText () {
		
		WebElement we = driver.findElement(By.linkText("This is a link"));
		System.out.println(we.getText());
		we.click();
	}
	
	@Test
	public void testTheTextboxAndByPartialLinkText () {
		
		WebElement we = driver.findElement(By.partialLinkText("This is a"));
		System.out.println(we.getText());
		we.click();
	}
	
	@Test
	public void testTheTextboxAndByCssSelector () {
		//locating web elements through CSS involves use of CSS Selector 
		//which identifies an element based on the combination of 
		//HTML tag, id, class and attributes.
		//1.Tag and Id -> #
		//2.Tag and Class -> .
		//3.Tag and attribute  -> tagname[attribute]
		//4.Tag, class and attribute ->tagname.class[attribute]
		//5.Sub-String matches
		
		//1.Tag and Id (tagname#id)
		/*WebElement we = driver.findElement(By.cssSelector("input#fname"));
		we.sendKeys("Check the tagnme with id test"); 
		
		WebElement web = driver.findElement(By.cssSelector("button#idOfButton"));
		web.click();  
		*/
		
		
		//2.Tag and Class (tagname.class) - uses dot to concatenate
		//driver.findElement(By.cssSelector("input.Automation")).click();  
		
		
		//3.Tag and attribute
		//driver.findElement(By.cssSelector("input[id=fname]")).sendKeys("Selenium Tutorials");  
		//driver.findElement(By.cssSelector("button[id=idOfButton]")).click(); 
		
		//4.Tag, class and attribute 
		/*
		driver.get("www.google.co.in");  //https://www.google.co.in/
		driver.findElement(By.cssSelector("input.gsfi[name=q]")).sendKeys("javaTpoint Tutorials"); 
		driver.findElement(By.cssSelector("input.gNO89b[name=btnK]")).click(); 
		*/
		
		//5.Sub-String matches
		//WebDriver provides an interesting feature of allowing partial string matches using ^, $ and *.
		
		// ^ means start with
		/*driver.findElement(By.cssSelector("input[id^='fna']")).sendKeys("JavaTpoint JMeter Tutorial"); 
		driver.findElement(By.cssSelector("button[id^='idOf']")).click(); 
		*/
		
		//$ which means 'ends with'.
		/*driver.findElement(By.cssSelector("input[id$='me']")).sendKeys("JavaTpoint Data Structure Tutorial");  
		driver.findElement(By.cssSelector("button[id$='on']")).click();  
		*/
		
		//* which means 'sub-string'.
		driver.findElement(By.cssSelector("input[id*='na']")).sendKeys("JavaTpoint C++ Tutorial");  
		driver.findElement(By.cssSelector("button[id*='Of']")).click(); 
	}
	
	
	
	
	
	
	
	
	//@AfterClass
	@AfterTest
	public void tearDwon() throws InterruptedException{
		Thread.sleep(1000);
		driver.quit();
	}

}
