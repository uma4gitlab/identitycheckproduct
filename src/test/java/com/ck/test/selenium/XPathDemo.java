package com.ck.test.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class XPathDemo {
	
	private WebDriver driver;
	
	//@BeforeTest
	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "/Users/chittranjankumar/Documents/Ckumar/Selenium/Driver/84.0.4147.30/chromedriver");
		driver = new ChromeDriver();
		driver.get("https://www.testandquiz.com/selenium/testing.html");
	}
	
	//XPath allows you to select individual elements, attributes, 
	//and some other part of XML documents for specifying location of a particular web element.
	
//	@Test
//	public void testTheTextboxUsingText () {
//		
//		WebElement we = driver.findElement(By.xpath("//b[text()='This is sample text.']"));
//		System.out.println(we.getText());
//		//we.click();
//	}
	
	@Test
	public void testTheTextboxUsingContains () {
		
		WebElement we = driver.findElement(By.xpath("//*[contains(text(),'This is a')]"));
		System.out.println(we.getText());
		we.click();
	}
	
	
	
	@AfterClass
	//@AfterTest
	public void tearDwon() throws InterruptedException{
		Thread.sleep(1000);
		driver.quit();
	}

}
