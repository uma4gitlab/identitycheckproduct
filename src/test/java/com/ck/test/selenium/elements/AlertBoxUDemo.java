package com.ck.test.selenium.elements;

import java.util.Set;

import javax.security.auth.callback.ConfirmationCallback;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Navigation;
import org.openqa.selenium.WebDriver.TargetLocator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AlertBoxUDemo {

	private WebDriver driver;

	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "C:\\Uma\\SeleniumJARs\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.testandquiz.com/selenium/testing.html");
		//https://rahulshettyacademy.com/AutomationPractice/  
	}

	@Test
	public void testForAlert() throws InterruptedException{
		driver.findElement(By.xpath("/html/body/div/div[11]/div/p/button")).click();
		
		Alert a=driver.switchTo().alert();
		Assert.assertEquals(a.getText(),"hi, JavaTpoint Testing");
		Thread.sleep(2000);
		a.accept();
	}
	
	@Test
    public void testForConfirmBox() throws InterruptedException{
		driver.findElement(By.xpath("/html/body/div/div[12]/div/p[1]/button")).click();
		Alert c=driver.switchTo().alert();
		Assert.assertEquals(c.getText(),"Press a button!");
		
		//c.accept();
		c.dismiss();
		Thread.sleep(2000);
	}
	
	@AfterClass
	public void tearDwon() throws InterruptedException {
		Thread.sleep(3000);
		driver.quit();
	}
}
