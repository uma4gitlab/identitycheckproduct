package com.ck.test.selenium.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class RadioDemo {

	private WebDriver driver;

	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "C:\\Uma\\SeleniumJARs\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.testandquiz.com/selenium/testing.html");
		
	}
	@Test
	public void dropdown()
	{
		WebElement we=driver.findElement(By.id("testingDropdown"));
		Select s=new Select(we);
		//s.selectByVisibleText("Database Testing");
		//s.selectByIndex(2);
		s.selectByValue("Performance");
	}
	

//	@Test
//	public void testForRadio() {
//
//		//WebElement we = driver.findElement(By.xpath("//*[@id=\'male\']"));
//		WebElement w=driver.findElement(By.xpath("//*[@id=\'female\']"));
//		w.click();
//		
//		//s.selectByVisibleText("Database Testing");
//		//s.selectByIndex(0);
//		//s.selectByVisibleText("Manual Testing");
//		//System.out.println(s.isMultiple());
//		//System.out.println(s.toString());
//	}
	
	@Test
	public void testForRadio() {

		WebElement we = driver.findElement(By.xpath("//*[@id=\'male\']"));
		System.out.println(we.getTagName());
		System.out.println(we.getText());
	}
	
	
	@Test
	public void testForRadiomethod2() {

    int  size = driver.findElements(By.xpath("//input[@name='gender']")).size();
    System.out.println(size);
    for(int i=0;i<size;i++) {
    	//driver.findElements(By.xpath("//input[@name='gender']")).get(1).click();
    	String t=driver.findElements(By.xpath("//input[@name='gender']")).get(i).getText();
    	System.out.println(t);
    }
	}
	@AfterClass
	public void tearDwon() throws InterruptedException {
		Thread.sleep(3000);
		driver.quit();
	}
}
