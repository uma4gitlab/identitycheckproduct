package com.ck.test.selenium.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class FrameDemo {
	private WebDriver driver;

	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "C:\\Uma\\SeleniumJARs\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.amazon.com");
	}
   // @Test
   // public void mouseoverframe()
//    {
  //   Actions a=new Actions(driver);
   //  a.moveToElement(driver.findElement(By.xpath("//*[@id=\'nav-link-accountList\']/span[1]"))).build().perform();
   // }
    @Test
    public void eventintext()
    {
    	 Actions a=new Actions(driver);
        WebElement move=driver.findElement(By.xpath("//*[@id=\'nav-link-accountList\']/span[1]"));
        a.moveToElement(driver.findElement(By.id("twotabsearchtextbox"))).click().keyDown(Keys.SHIFT).sendKeys("hello").build().perform();
        a.moveToElement(move).build().perform();
    }
    
}
