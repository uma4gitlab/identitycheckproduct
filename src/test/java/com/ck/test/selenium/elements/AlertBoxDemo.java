package com.ck.test.selenium.elements;

import java.util.Set;

import javax.security.auth.callback.ConfirmationCallback;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Navigation;
import org.openqa.selenium.WebDriver.TargetLocator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AlertBoxDemo {

	private WebDriver driver;

	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver",
				"/Users/chittranjankumar/Documents/Ckumar/Selenium/Driver/84.0.4147.30/chromedriver");
		driver = new ChromeDriver();
		driver.get("https://www.testandquiz.com/selenium/testing.html");
		//https://rahulshettyacademy.com/AutomationPractice/  
	}

	@Test
	public void testForAlert() throws InterruptedException{
		
		WebElement we = driver.findElement(By.xpath("/html/body/div/div[11]/div/p/button"));
		we.click();
		//Thread.sleep(2000);
		Alert a = driver.switchTo().alert();
		//System.out.println(a.getText());body > div > div:nth-child(20) > div > p > button
		
		Assert.assertEquals(a.getText(), "hi, JavaTpoint Testing");
		a.accept();
	}
	
	@Test
	public void testForConfirmBox() throws InterruptedException{
		
		driver.findElement(By.xpath("/html/body/div/div[12]/div/p[1]/button")).click();
		//Thread.sleep(2000);
		Alert a = driver.switchTo().alert();
		//a.sendKeys("I am sending a key to you alert bhai");
		//System.out.println(a.getText());body > div > div:nth-child(20) > div > p > button
		
		Assert.assertEquals(a.getText(), "Press a button!");
		a.accept();
	}
	
	@AfterClass
	public void tearDwon() throws InterruptedException {
		Thread.sleep(3000);
		driver.quit();
	}
}
