package com.ck.test.selenium.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DropDownDemo {

	private WebDriver driver;

	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver",
				"/Users/chittranjankumar/Documents/Ckumar/Selenium/Driver/84.0.4147.30/chromedriver");
		driver = new ChromeDriver();
		//driver.get("https://www.testandquiz.com/selenium/testing.html");
	}

	@Test
	public void testTheDropDwon() {

		WebElement we = driver.findElement(By.id("testingDropdown"));
		Select s = new Select(we);
		//s.selectByVisibleText("Database Testing");
		//s.selectByIndex(0);
		s.selectByVisibleText("Manual Testing");
		System.out.println(s.isMultiple());
		//System.out.println(s.toString());
	}
	

	@AfterClass
	public void tearDwon() throws InterruptedException {
		Thread.sleep(3000);
		driver.quit();
	}
}
