package com.ck.test.selenium.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DragAndDropDemo {

	private WebDriver driver;

	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver",
				"/Users/chittranjankumar/Documents/Ckumar/Selenium/Driver/84.0.4147.30/chromedriver");
		driver = new ChromeDriver();
		driver.get("https://www.testandquiz.com/selenium/testing.html");
	}
	
	@Test
	public void testForDragAndDrop() throws InterruptedException{
			
//		clickAndHold(WebElement element) - Clicks a web element at the middle(without releasing).
//		moveToElement(WebElement element) - Moves the mouse pointer to the middle of the web element without clicking.
//		release(WebElement element) - Releases the left click (which is in pressed state).
//		build() - Generates a composite action
		
			WebElement from = driver.findElement(By.id("sourceImage"));
			WebElement to = driver.findElement(By.id("targetDiv"));
			
			Actions actions = new Actions(driver);
			actions.dragAndDrop(from, to).build().perform();

			//OR
//			Actions actions = new Actions(driver);
//			actions.clickAndHold(from);
//			actions.moveToElement(to);
//			actions.release(to);
//			actions.build().perform();
	}


	
	@AfterClass
	public void tearDwon() throws InterruptedException {
		Thread.sleep(5000);
		driver.quit();
	}
}
