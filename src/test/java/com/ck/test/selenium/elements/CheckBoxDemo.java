package com.ck.test.selenium.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CheckBoxDemo {

	private WebDriver driver;

	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "C:\\Uma\\SeleniumJARs\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.testandquiz.com/selenium/testing.html");
	}
	@Test
	public void testForRadio1() {

	  WebElement w = driver.findElement(By.cssSelector("input[value='Performance']"));
	  if(!w.isSelected())
		  w.click();
	  
	  WebElement w1 = driver.findElement(By.cssSelector("input[value='Automation']"));
	  if(!w1.isSelected())
		  w1.click();
	  
	}
	
	
	
  @AfterClass
	public void tearDwon() throws InterruptedException {
		Thread.sleep(3000);
		driver.quit();
	}
}
