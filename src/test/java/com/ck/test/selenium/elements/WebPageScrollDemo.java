package com.ck.test.selenium.elements;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WebPageScrollDemo {

	private WebDriver driver;

	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "C:\\Uma\\SeleniumJARs\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.testandquiz.com/selenium/testing.html");
	}
	
	@Test
	public void testForwebPageScroll() throws InterruptedException{
			
//		To scroll a web page, we have to use the scrollBy method of JavaScript. 
//		For executing the JavaScript method we will use JavaScript executor. 
//		The scrollBy method takes two parameters one each for horizontal and vertical scroll in terms of pixels.
		
		//JavascriptExecutor js = (JavascriptExecutor)driver;  
		//js.executeScript("scrollBy(0, 300)");  
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("scrollBy(0,100)");	
	}


	
	@AfterClass
	public void tearDwon() throws InterruptedException {
		Thread.sleep(5000);
		driver.quit();
	}
}
