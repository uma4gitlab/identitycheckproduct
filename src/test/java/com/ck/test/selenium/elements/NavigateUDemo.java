package com.ck.test.selenium.elements;

import java.util.Set;

import javax.security.auth.callback.ConfirmationCallback;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Navigation;
import org.openqa.selenium.WebDriver.TargetLocator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class NavigateUDemo {

	private WebDriver driver;

	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "C:\\Uma\\SeleniumJARs\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		//driver.get("https://www.google.com");
	}

	@Test ()
	public void testForNavigate1() throws InterruptedException{

		Navigation n = driver.navigate();
		n.to("https://www.google.com");   
		driver.findElement(By.name("q")).sendKeys("selenium Tutorial"); 
        driver.findElement(By.name("btnK")).submit();
        n.refresh();
        Thread.sleep(2000);
	    n.back();
	    Thread.sleep(2000);
	    n.forward();
	
	}
	
	@AfterClass
	public void teardown() throws InterruptedException {
	  Thread.sleep(2000);	
	  driver.quit();	
	}
	

}
