package com.ck.test.selenium.elements;

import java.util.Set;

import javax.security.auth.callback.ConfirmationCallback;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Navigation;
import org.openqa.selenium.WebDriver.TargetLocator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class NavigateDemo {

	private WebDriver driver;

	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver",
				"/Users/chittranjankumar/Documents/Ckumar/Selenium/Driver/84.0.4147.30/chromedriver");
		driver = new ChromeDriver();
		//driver.get("https://www.testandquiz.com/selenium/testing.html");
	}

	@Test (enabled = false)
	public void testForNavigate1() throws InterruptedException{

		Navigation n = driver.navigate();
		n.to("https://www.testandquiz.com/selenium/testing.html");   
		WebElement we = driver.findElement(By.linkText("This is a link")); we.click(); 
		n.refresh();
		Thread.sleep(3000);
		n.back();   
		Thread.sleep(3000);
		n.forward();  
		Thread.sleep(3000);
	}

	@Test (enabled = false)
	public void testForNavigate() throws InterruptedException{

		Navigation n = driver.navigate();
		n.to("https://www.testandquiz.com/selenium/testing.html");   
		WebElement we = driver.findElement(By.linkText("This is a link")); we.click(); 
		n.refresh();
		Thread.sleep(3000);
		n.back();   
		Thread.sleep(3000);
		n.forward();  
		Thread.sleep(3000);
		
		driver.navigate().to("https://www.testandquiz.com/selenium/testing.html");   
		driver.findElement(By.linkText("This is a link")).click(); 
		driver.navigate().refresh();
		Thread.sleep(3000);
		driver.navigate().back();   
		Thread.sleep(3000);
		driver.navigate().forward();  
		Thread.sleep(3000);
	}
	
	
	@Test
	public void testForNavigateGoogle() throws InterruptedException{
		Navigation n = driver.navigate();
		n.to("https://www.google.com/");
		WebElement we = driver.findElement(By.name("q"));
		we.sendKeys("Selenium for xpath tutorial ");
		driver.findElement(By.name("btnK")).submit();
		
		n.refresh();
		n.back();
		
		n.forward();
		n.refresh();
		
		//Assert.assertNotNull(we);
		//Assert.assertNotNull(n);
		
		TargetLocator tl = driver.switchTo();//.alert().accept();  
		Alert a = tl.alert();
		Alert aa = (Alert) driver.switchTo().alert();
		
		
	}

//	Test
//	void testCloseAllWindowsExceptOneNotMobile()
//	{
//	    when(webDriverProvider.get()).thenReturn(webDriver);
//	    Set<String> windows = new LinkedHashSet<>(List.of(WINDOW1, WINDOW2));
//	    TargetLocator targetLocator = mock(TargetLocator.class);
//	    when(webDriver.getWindowHandles()).thenReturn(windows);
//	    when(webDriver.switchTo()).thenReturn(targetLocator);
//	    Options options = mock(Options.class);
//	    Window window = mock(Window.class);
//	    when(webDriver.manage()).thenReturn(options);
//	    when(options.window()).thenReturn(window);
//	    when(window.getSize()).thenAnswer(i -> new Dimension(DIMENSION_SIZE, DIMENSION_SIZE));
//	    when(webDriverManager.isAndroid()).thenReturn(false);
//	    windowsActions.closeAllWindowsExceptOne();
//	    verify(webDriver, times(1)).close();
//	}
	
	@AfterClass
	public void tearDwon() throws InterruptedException {
		Thread.sleep(3000);
		driver.quit();
	}
}
