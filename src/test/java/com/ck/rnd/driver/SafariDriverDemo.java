package com.ck.rnd.driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SafariDriverDemo {
	
	private WebDriver driver;
	
	@BeforeTest
	public void setUp() {
		driver = new SafariDriver();
		driver.get("https://www.testandquiz.com/selenium/testing.html");
	}
	
	@Test
	public void testTheTextboxAndByPartialLinkText () {
		
		WebElement we = driver.findElement(By.partialLinkText("This is a"));
		System.out.println(we.getText());
		we.click();
	}
	
	@Test
	public void testTheTextboxAndByCssSelector () {
		//* which means 'sub-string'.
		driver.findElement(By.cssSelector("input[id*='na']")).sendKeys("JavaTpoint C++ Tutorial");  
		driver.findElement(By.cssSelector("button[id*='Of']")).click(); 
	}
	
	//@AfterClass
	@AfterTest
	public void tearDwon() throws InterruptedException{
		Thread.sleep(1000);
		driver.quit();
	}

}
