package com.ck.rnd.driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class IEDriverDemo {
	
	private WebDriver driver;
	
	@BeforeTest
	public void setUp() {//uma's comment
		System.setProperty("webdriver.ie.driver", "/Users/chittranjankumar/Documents/Ckumar/Selenium/Driver/IEDriverServer.exe");
		driver = new InternetExplorerDriver();
		driver.get("https://www.testandquiz.com/selenium/testing.html");
	}
	
	@Test
	public void testTheTextboxAndByPartialLinkText () {
		
		WebElement we = driver.findElement(By.partialLinkText("This is a"));
		System.out.println(we.getText());
		we.click();
	}
	
	@Test
	public void testTheTextboxAndByCssSelector () {
		//* which means 'sub-string'.
		driver.findElement(By.cssSelector("input[id*='na']")).sendKeys("JavaTpoint C++ Tutorial");  
		driver.findElement(By.cssSelector("button[id*='Of']")).click(); 
	}
	
	//@AfterClass
	@AfterTest
	public void tearDwon() throws InterruptedException{
		Thread.sleep(1000);
		driver.quit();
	}

}
